# ffmpeg-avbuild

This repository contains build scripts that can be used to build ffmpeg libraries.

The build scripts is from https://github.com/wang-bin/avbuild

## How to build
Please see: https://github.com/wang-bin/avbuild/wiki

## Reference & Acknowledgment
[avbuild](https://github.com/wang-bin/avbuild) - It's a tool to build ffmpeg for almost all platforms.

## License
LGPL-3.0 license

